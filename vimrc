set nocompatible                " Use Vim defaults instead of 100% vi compatibility
set backspace=indent,eol,start  " more powerful backspacing

set history=50                  " keep 50 lines of command line history
set ruler                       " show the cursor position all the time

set t_Co=256 " always use 256 colors
colorscheme lucius
LuciusBlack
syntax on

" indentation: 3-wide tabs
set tabstop=3
set shiftwidth=3
filetype plugin indent on

" show tab characters and trailing spaces
set list listchars=tab:»·,trail:·

autocmd BufReadPost *
  \ if line("'\"") > 1 && line("'\"") <= line("$") |
  \   exe "normal! g`\"" |
  \ endif

" remap movement keys for keymap
set langmap=hk,jh,kj

set mouse=a

" fish doesn't play well with vim
set shell=/bin/sh

let g:sudo_no_gui=1
let g:python_recommended_style=0
