#!/bin/sh
set -e
echo 'Installing fish config'
install -D config.fish ~/.config/fish/config.fish
echo 'Installing npmrc'
install -D npmrc ~/.npmrc
echo 'Installing SSH config/authorized_keys'
install -D ssh_config ~/.ssh/config
install -Dm 600 authorized_keys ~/.ssh/authorized_keys
echo 'Installing git config'
install gitconfig ~/.gitconfig
echo 'Installing vim setup'
install -D vimrc ~/.vimrc
install -D lucius.vim ~/.vim/colors/lucius.vim
install -D localvimrc.vim ~/.vim/plugin/localvimrc.vim
vim -c 'so % | q' SudoEdit.vmb
