set fish_greeting ""

set PATH /usr/local/bin /usr/local/sbin /usr/bin /usr/sbin $HOME/.local/bin

function fish_prompt
	set -l last_status $status

	if test $last_status -ne 0
		set_color normal --bold
		printf '[%d] ' $last_status
	end

	set_color yellow
	printf '%s ' (prompt_pwd)
	set_color normal
end

set -q HOST_COLOR; or set HOST_COLOR red
function fish_right_prompt
	set_color $HOST_COLOR
	printf '%s' $USER
	set_color normal
end

set -x EDITOR vim

alias du "du --si"
alias df "df --si"
alias ls "ls --si --color=auto"
alias journalctl "journalctl -n 1000"
